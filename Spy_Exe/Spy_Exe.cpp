﻿// Spy_Exe.cpp : Defines the entry point for the application.
//
// Class 16CLC1 _ FIT.HCMUS
// Group 8
// StudentID :
//      1653010 PHAM VAN CHUAN
//      1653006 HUYNH THANH BINH
//      1653011 VU MINH DANG
//      1653020 VU HAI
//      1653070 PHAN NHAT QUANG
//
//===========================================================

#include "stdafx.h"
#include "Spy_Exe.h"
#include <Windowsx.h>
#include "User_Interface.h"

#define MAX_LOADSTRING 128

#define IMPORT __declspec(dllimport)
IMPORT void copyLinkToExe(WCHAR* path_tmp);
IMPORT void test();

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HINSTANCE hinstDLL;
HHOOK hHook = NULL;


typedef VOID(*LOADPROC)(HHOOK hHook);
void doHook(HWND hWnd, int type);

INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    MainDlgProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	hInst = hInstance;
	HWND hDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_MAIN), NULL, &MainDlgProc);

	if (!hDlg) {
		MessageBox(NULL, L"Could not create Dialog Window!", L"Create Dialog", MB_ICONERROR);
		return FALSE;
	}

	ShowWindow(hDlg, nCmdShow);
	UpdateWindow(hDlg);

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SPY_EXE));

	MSG msg;

	//   while (GetMessage(&msg, nullptr, 0, 0))
	//   {
	//       if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
	//       {
	//           TranslateMessage(&msg);
	//           DispatchMessage(&msg);
	//       }
	//   }

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (TranslateAccelerator(hDlg, hAccelTable, &msg)) {
			continue;
		}
		if (!IsDialogMessage(hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	} return (int)msg.wParam;
}

INT_PTR CALLBACK MainDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_INITDIALOG: {
		HICON hIcon;
		hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_MAIN_ICON));
		if (hIcon) SendMessage(hDlg, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);

		EnableWindow(GetDlgItem(hDlg, IDC_PATH), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_INSTALL), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_REMOVE), FALSE);
		DeleteObject(hIcon);
	} return (INT_PTR)TRUE;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam)) { // lấy 2 byte thấp của WordParam --> cho biết ID của control;
		case IDCANCEL:
			doHook(hDlg, 2);
			PostQuitMessage(0);
			break;
		case IDC_BUTTON_CHOOSE: {
			WCHAR * lpszDirectory = NULL;
			if (BrowseForFolder(hDlg, lpszDirectory) == TRUE) {
				copyLinkToExe(lpszDirectory);
				//test();
				SetDlgItemText(hDlg, IDC_PATH, lpszDirectory);
				EnableWindow(GetDlgItem(hDlg, IDC_PATH), FALSE);
				EnableWindow(GetDlgItem(hDlg, IDC_INSTALL), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_REMOVE), FALSE);
				delete[]lpszDirectory;
			}
			else {
				delete[]lpszDirectory;
				lpszDirectory = NULL;
				test();
			}
		} break;
		case IDC_INSTALL:
			EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_CHOOSE), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_INSTALL), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_REMOVE), TRUE);
			doHook(hDlg, 1);
			break;
		case IDC_REMOVE:
			EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_CHOOSE), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_INSTALL), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_REMOVE), FALSE);
			doHook(hDlg, 2);
			break;
		case IDC_ABOUTUS:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hDlg, About);
			break;
		case IDC_TRAY:
			doMinimize(hInst, hDlg);
			break;
		} return (INT_PTR)TRUE;
	}
	case MY_WM_NOTIFYICON:
		doMyNotify(hDlg, wParam, lParam);
		break;
	} return (INT_PTR)FALSE;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
	case WM_INITDIALOG: {
		HICON hIcon;
		hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_MAIN_ICON));
		if (hIcon) SendMessage(hDlg, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
		DeleteObject(hIcon);
	} return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        } break;
    } return (INT_PTR)FALSE;
}

void doHook(HWND hWnd, int type) {
	// call DLL function by run-time way
	typedef VOID(*MYPROC)(HWND);

	MYPROC ProcAddr;
	hinstDLL = LoadLibrary(L"spy.dll");

	if (type == 1) {
		if (hinstDLL != NULL) {
			ProcAddr = (MYPROC)GetProcAddress(hinstDLL, "_doInstallHook");
			if (ProcAddr != NULL) {
				LOADPROC lpr = (LOADPROC)GetProcAddress(hinstDLL, "SetGlobalHook");
				lpr(hHook);
				ProcAddr(hWnd);
			}
			else { 
				MessageBox(hWnd, L"Cannot get the address of the function!", L"Error", MB_OK); 
			}
		}
		else {
			MessageBox(hWnd, L"Cannot load DLL library!", L"Error", MB_OK);
		}
	}
	else if (type == 2) {
		ProcAddr = (MYPROC)GetProcAddress(hinstDLL, "_doRemoveHook");
		if (ProcAddr != NULL) {
			ProcAddr(hWnd);
		}
		else { 
			MessageBox(hWnd, L"Cannot get the address of the function!", L"Error", MB_OK); 
		}
	} FreeLibrary(hinstDLL);
}
