//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Spy_Exe.rc
//

#define MY_WM_NOTIFYICON				WM_USER+1
#define IDC_MYICON                      2
#define IDD_SPY_EXE_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDI_SPY_EXE                     107
#define IDI_SMALL                       108
#define IDC_SPY_EXE                     109
#define IDR_MAINFRAME                   128
#define IDD_MAIN                        129
#define IDB_MAIN                        130
#define IDI_MAIN_ICON                   131
#define IDB_ABOUT                       132
#define IDC_PATH                        1000
#define IDC_BUTTON_CHOOSE               1001
#define IDC_REMOVE                      1004
#define IDC_TRAY                        1005
#define IDC_INSTALL                     1006
#define IDC_ABOUTUS                     1007
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
