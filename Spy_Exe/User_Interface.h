#include "stdafx.h"

int CALLBACK BrowseCallbackProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM pData);
BOOL BrowseForFolder(HWND hWnd, WCHAR* &lpszDirectory);
WCHAR* convertPath(WCHAR* lpszDirectory);


BOOL MyTaskBarAddIcon(HWND hWnd, UINT uID, HICON hIcon, LPCWSTR lpszTip);
void doMinimize(HINSTANCE hInst, HWND hWnd);
void doMyNotify(HWND hWnd, WPARAM wParam, LPARAM lParam);
BOOL MyTaskBarDeleteIcon(HWND hWnd, UINT uID);