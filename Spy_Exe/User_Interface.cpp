﻿#include "stdafx.h"
#include <shlobj_core.h>
#include <shellapi.h>
#include "User_Interface.h"
#include "Resource.h"


int CALLBACK BrowseCallbackProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM pData) {
	LPCTSTR pszRootDir = (LPCTSTR)pData;
	switch (uMsg) {
	case BFFM_INITIALIZED: {
		SendMessage(hWnd, BFFM_SETSELECTION, TRUE, (LPARAM)pszRootDir);
		break;
	}
	case BFFM_SELCHANGED: {
		LPTSTR pszSelDir = new TCHAR[MAX_PATH];
		if (SHGetPathFromIDList((LPITEMIDLIST)lParam, pszSelDir)) {
			SendMessage(hWnd, BFFM_SETSTATUSTEXT, 0, (LPARAM)pszSelDir);
		} delete[] pszSelDir;
	} break;
	default:
		break;
	} return 0;
}

BOOL BrowseForFolder(HWND hWnd, WCHAR* &lpszDirectory)
{
	LPMALLOC lpMalloc;
	LPITEMIDLIST pidl;
	HRESULT hResult = SHGetSpecialFolderLocation(NULL, CSIDL_DRIVES, &pidl);

	if (FAILED(hResult))                      return FALSE;    
	if (SHGetMalloc(&lpMalloc) != NOERROR)    return FALSE;

	WCHAR szDisplayName[MAX_PATH]; // roi
	lpszDirectory = new WCHAR[MAX_PATH];
	ZeroMemory(lpszDirectory, sizeof(lpszDirectory));


	BROWSEINFO info;
	if (IsWindow(hWnd)) info.hwndOwner = hWnd;
	else                info.hwndOwner = NULL;

	info.pidlRoot = pidl;
	info.pszDisplayName = szDisplayName;
	info.lpszTitle = L"Choose containing folder: ";
	info.ulFlags = BIF_DONTGOBELOWDOMAIN | BIF_STATUSTEXT;
	info.lpfn = BrowseCallbackProc;
	info.lParam = (LPARAM)lpszDirectory;
	info.iImage = 0;

	/*if (wcslen(lpszDirectory) == 0) {
	MessageBox(NULL, L"wcslen = 0", L"LENGTH NOTICE", MB_OK);
	} */

	LPITEMIDLIST lpItemIDList;

	// select the folder
	if ((lpItemIDList = SHBrowseForFolder(&info)) != NULL) {
		// use the return information to obtain the full folder path
		if (SHGetPathFromIDList(lpItemIDList, lpszDirectory)) {
			if (lpszDirectory[0] == '\0') {
				MessageBox(NULL, L"Find directory failed  ! ", L"NOTICE", MB_ICONSTOP | MB_OK);
				return FALSE;
			}
		}
		else {
			MessageBox(NULL, L"Find directory failed !!! ", L"NOTICE", MB_ICONSTOP | MB_OK);
			BrowseForFolder(hWnd, lpszDirectory);
			lpMalloc->Free(lpItemIDList);
			lpMalloc->Release();
			return TRUE;
		}

		wsprintf(lpszDirectory, convertPath(lpszDirectory)); // new directory 
		lpMalloc->Free(lpItemIDList);
		lpMalloc->Release();
		return TRUE;
	} return FALSE;
}

WCHAR* convertPath(WCHAR* lpszDirectory) {
	WCHAR tmp[MAX_PATH];
	wsprintf(tmp, lpszDirectory);
	wsprintf(tmp, wcscat(wcstok(tmp, L"\\"), L"\\"));
	wsprintf(tmp, wcscat(tmp, wcsstr(lpszDirectory, L"\\")));
	return tmp;
}

BOOL MyTaskBarAddIcon(HWND hWnd, UINT uID, HICON hIcon, LPCWSTR lpszTip) {
	BOOL result;
	NOTIFYICONDATA tnid;

	tnid.cbSize = sizeof(NOTIFYICONDATA);
	tnid.hWnd = hWnd;
	tnid.uID = uID;
	tnid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	tnid.uCallbackMessage = MY_WM_NOTIFYICON;
	tnid.hIcon = hIcon;

	if (lpszTip) lstrcpyn(tnid.szTip, lpszTip, sizeof(tnid.szTip));
	else         tnid.szTip[0] = '\0';

	result = Shell_NotifyIcon(NIM_ADD, &tnid);

	if (hIcon) DestroyIcon(hIcon);
	return result;
}

void  doMinimize(HINSTANCE hInst, HWND hWnd) {
	ShowWindow(hWnd, SW_HIDE);
	HICON hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_MAIN_ICON));
	MyTaskBarAddIcon(hWnd, IDI_MAIN_ICON, hIcon, (LPCWSTR)L"Keyboard Spy App\nClick to restore !");
}

void doMyNotify(HWND hWnd, WPARAM wParam, LPARAM lParam) {
	if (lParam == WM_LBUTTONDOWN) {
		MyTaskBarDeleteIcon(hWnd, IDI_MAIN_ICON);
		ShowWindow(hWnd, SW_SHOW);
		ShowWindow(hWnd, SW_RESTORE);
	}
}

BOOL MyTaskBarDeleteIcon(HWND hWnd, UINT uID) {
	BOOL result;
	NOTIFYICONDATA tnid;

	tnid.cbSize = sizeof(NOTIFYICONDATA);
	tnid.hWnd = hWnd;
	tnid.uID = uID;

	result = Shell_NotifyIcon(NIM_DELETE, &tnid);
	return result;
}
